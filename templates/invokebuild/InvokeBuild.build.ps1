<#
.Synopsis
	Sample build script with automatic bootstrapping.

.Example
	PS> ./Project.build.ps1 Build

	This command invokes the task Build defined in this script.
	The required packages are downloaded on the first call.
	Then Build is invoked by local Invoke-Build.

.Example
	PS> Invoke-Build Build

	It also invokes the task Build defined in this script. But:
	- It is invoked by global Invoke-Build.
	- It does not check or install packages.
#>

param(
	[Parameter(Position=0)]
	$Tasks,
    $StageArea,
    [string[]]
    $PublishProjects = @("console", "web"),
	[Switch]
    $SetSemver,
    [Switch]
    $NoToolRestore
)

function Test-DotnetAvailable {
	if(!(Get-Command dotnet -ErrorAction Ignore)){
		Write-Error "dotnet is missing. Install dotnet core sdk"
		Exit(1)
	}
}

function Assert-ToolExists {
    param(
        [string[]]
        $tools,
        [string]
        $toolName
    )
    $tools | Where-Object { $_ -match $toolName }
}
function Install-ToolsIfNeeded {
	if(!(Test-Path ".config\dotnet-tools.json")) {
		& dotnet new tool-manifest 
	}
	$tools = & dotnet tool list --local
	if(!(Assert-ToolExists $tools "paket")) {
		& dotnet tool install paket
    }
    if(!(Assert-ToolExists $tools "gitversion.tool")) {
        & dotnet tool install gitversion.tool
    }
    & dotnet tool restore
    
}

function Initialize-PaketIfNeeded {
    if(!(Test-Path "$PSScriptRoot\paket.dependencies")) {
        & dotnet paket init
    }
    # if(!(Test-Path "$PSScriptRoot\paket.lock")) {
    #     & dotnet paket install
    # }

}
function Test-InvokeBuildInPaket {
	$packages = & dotnet paket "show-installed-packages"
	$ibpackage = $packages | where-object { $_ -match "Invoke-Build" }
	if(!$ibpackage) {
		Write-Host -ForegroundColor Red 'To complete invoke-build init: add invoke-build to paket.dependencies:'
		Write-Host -ForegroundColor Yellow 'nuget Invoke-Build storage:packages'
		exit 1
	}
}

# Direct call: ensure packages and call the local Invoke-Build

if ([System.IO.Path]::GetFileName($MyInvocation.ScriptName) -ne 'Invoke-Build.ps1') {
	Test-DotnetAvailable
	$ErrorActionPreference = 'Stop'
    $ib = "$PSScriptRoot/packages/Invoke-Build/tools/Invoke-Build.ps1"
    $config = "$PSScriptRoot/.config/dotnet-tools.json"

	# install packages
	if (!(Test-Path -LiteralPath $ib) -or !(Test-Path -LiteralPath $config)) {
		'Validating tools'
        Install-ToolsIfNeeded
        Initialize-PaketIfNeeded
		'Installing packages...'
        
		& dotnet paket install
        if ($LASTEXITCODE) {throw "paket exit code: $LASTEXITCODE"}
        
		Test-InvokeBuildInPaket
	}

	# call Invoke-Build
	& $ib -Task $Tasks -File $MyInvocation.MyCommand.Path @PSBoundParameters
	return
}

# Normal call for tasks, either by local or global Invoke-Build


# Synopsis: Build Solution
task Build ToolRestore, SetSemver, {
	exec { & dotnet build --runtime win-x64 }
}

# Synopsis: Publish projects listed in PublishProjects
task Publish Build, Test, {
	$PublishProjects |
	ForEach-Object { 
		if($StageArea) {
			exec { & dotnet publish --self-contained --runtime win-x64 --output "$StageArea\$_" --no-build "src\$_" } 
		}	else {
			exec { & dotnet publish --self-contained --runtime win-x64 --no-build "src\$_" } 
		}	
	}
}

# Synopsis: Run automated tests.
task Test Build, {
	"Running tests..."
	exec { & dotnet test }
}

# Synopsis: Update assemblyinfo with semver from git.
task SetSemver -If $SetSemver {
	exec { & dotnet gitversion -updateassemblyinfo }
	exec { & dotnet gitversion -output buildserver }
}

# Synopsis: Restore or if paket.lock is missing: install and restore. 
task ToolRestore -If (!$NoToolRestore) {
	exec { & dotnet tool restore }
}

# Synopsis: Install packages explicitly.
task PaketInstall {
	exec { & dotnet paket install }
}

# Synopsis: Remove standard dotnet build output.
task Clean {
	Remove-Item packages, paket-files -Force -Recurse -ErrorAction 2
	exec { & dotnet clean }
}

# Synopsis: git clean -Xdf
task GitClean Clean, { 
	exec { & git clean -Xdf }
}

task . Build
