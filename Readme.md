# Dotnet new template for Powershell build script framework Invoke-Build

This template will add the powershel script `InvokeBuild.build.ps1` to the 
root of the project.

The script bootstraps Invoke-Build semi-automatically if needed.
The script tell if dotnet core sdk is not installed.
